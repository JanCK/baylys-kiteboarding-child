require 'compass/import-once/activate'
# Require any additional compass plugins here.

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "/"
sass_dir = "scss"
images_dir = "../baylys/images"
javascripts_dir = "../baylys/js"
fonts_dir = "fonts"

output_style = :compressed

relative_assets = true

line_comments = false
color_output = false

preferred_syntax = :scss
