/* Custom Section */

/*
 Sticky-kit v1.1.2 | WTFPL | Leaf Corcoran 2015 | http://leafo.net
*/
(function(){var b,f;b=this.jQuery||window.jQuery;f=b(window);b.fn.stick_in_parent=function(d){var A,w,J,n,B,K,p,q,k,E,t;null==d&&(d={});t=d.sticky_class;B=d.inner_scrolling;E=d.recalc_every;k=d.parent;q=d.offset_top;p=d.spacer;w=d.bottoming;null==q&&(q=0);null==k&&(k=void 0);null==B&&(B=!0);null==t&&(t="is_stuck");A=b(document);null==w&&(w=!0);J=function(a,d,n,C,F,u,r,G){var v,H,m,D,I,c,g,x,y,z,h,l;if(!a.data("sticky_kit")){a.data("sticky_kit",!0);I=A.height();g=a.parent();null!=k&&(g=g.closest(k));
if(!g.length)throw"failed to find stick parent";v=m=!1;(h=null!=p?p&&a.closest(p):b("<div />"))&&h.css("position",a.css("position"));x=function(){var c,f,e;if(!G&&(I=A.height(),c=parseInt(g.css("border-top-width"),10),f=parseInt(g.css("padding-top"),10),d=parseInt(g.css("padding-bottom"),10),n=g.offset().top+c+f,C=g.height(),m&&(v=m=!1,null==p&&(a.insertAfter(h),h.detach()),a.css({position:"",top:"",width:"",bottom:""}).removeClass(t),e=!0),F=a.offset().top-(parseInt(a.css("margin-top"),10)||0)-q,
u=a.outerHeight(!0),r=a.css("float"),h&&h.css({width:a.outerWidth(!0),height:u,display:a.css("display"),"vertical-align":a.css("vertical-align"),"float":r}),e))return l()};x();if(u!==C)return D=void 0,c=q,z=E,l=function(){var b,l,e,k;if(!G&&(e=!1,null!=z&&(--z,0>=z&&(z=E,x(),e=!0)),e||A.height()===I||x(),e=f.scrollTop(),null!=D&&(l=e-D),D=e,m?(w&&(k=e+u+c>C+n,v&&!k&&(v=!1,a.css({position:"fixed",bottom:"",top:c}).trigger("sticky_kit:unbottom"))),e<F&&(m=!1,c=q,null==p&&("left"!==r&&"right"!==r||a.insertAfter(h),
h.detach()),b={position:"",width:"",top:""},a.css(b).removeClass(t).trigger("sticky_kit:unstick")),B&&(b=f.height(),u+q>b&&!v&&(c-=l,c=Math.max(b-u,c),c=Math.min(q,c),m&&a.css({top:c+"px"})))):e>F&&(m=!0,b={position:"fixed",top:c},b.width="border-box"===a.css("box-sizing")?a.outerWidth()+"px":a.width()+"px",a.css(b).addClass(t),null==p&&(a.after(h),"left"!==r&&"right"!==r||h.append(a)),a.trigger("sticky_kit:stick")),m&&w&&(null==k&&(k=e+u+c>C+n),!v&&k)))return v=!0,"static"===g.css("position")&&g.css({position:"relative"}),
a.css({position:"absolute",bottom:d,top:"auto"}).trigger("sticky_kit:bottom")},y=function(){x();return l()},H=function(){G=!0;f.off("touchmove",l);f.off("scroll",l);f.off("resize",y);b(document.body).off("sticky_kit:recalc",y);a.off("sticky_kit:detach",H);a.removeData("sticky_kit");a.css({position:"",bottom:"",top:"",width:""});g.position("position","");if(m)return null==p&&("left"!==r&&"right"!==r||a.insertAfter(h),h.remove()),a.removeClass(t)},f.on("touchmove",l),f.on("scroll",l),f.on("resize",
y),b(document.body).on("sticky_kit:recalc",y),a.on("sticky_kit:detach",H),setTimeout(l,0)}};n=0;for(K=this.length;n<K;n++)d=this[n],J(b(d));return this}}).call(this);


/*-----------------------------------------------------------------------------------
Polyfill RequestAnimationFrame
-----------------------------------------------------------------------------------*/
// http://paulirish.com/2011/requestanimationframe-for-smart-animating/
// http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
 
// requestAnimationFrame polyfill by Erik Möller
// fixes from Paul Irish and Tino Zijdel
 
(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame']
                                   || window[vendors[x]+'CancelRequestAnimationFrame'];
    }
 
    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); },
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };
 
    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());

/*-----------------------------------------------------------------------------------
 Smooth Scrolling
-----------------------------------------------------------------------------------*/
/*!
 * Smooth Scroll - v1.4.5 - 2012-07-21
 *
 * Copyright (c) 2012 Karl Swedberg; Licensed MIT, GPL
 * https://github.com/kswedberg/jquery-smooth-scroll
 *
 *
*/
(function(a){function f(a){return a.replace(/(:|\.)/g,"\\$1")}var b="1.4.5",c={exclude:[],excludeWithin:[],offset:0,direction:"top",scrollElement:null,scrollTarget:null,beforeScroll:function(){},afterScroll:function(){},easing:"swing",speed:400,autoCoefficent:2},d=function(b){var c=[],d=!1,e=b.dir&&b.dir=="left"?"scrollLeft":"scrollTop";return this.each(function(){if(this==document||this==window)return;var b=a(this);b[e]()>0?c.push(this):(b[e](1),d=b[e]()>0,b[e](0),d&&c.push(this))}),b.el==="first"&&c.length&&(c=[c[0]]),c},e="ontouchend"in document;a.fn.extend({scrollable:function(a){var b=d.call(this,{dir:a});return this.pushStack(b)},firstScrollable:function(a){var b=d.call(this,{el:"first",dir:a});return this.pushStack(b)},smoothScroll:function(b){b=b||{};var c=a.extend({},a.fn.smoothScroll.defaults,b),d=a.smoothScroll.filterPath(location.pathname);return this.unbind("click.smoothscroll").bind("click.smoothscroll",function(b){var e=this,g=a(this),h=c.exclude,i=c.excludeWithin,j=0,k=0,l=!0,m={},n=location.hostname===e.hostname||!e.hostname,o=c.scrollTarget||(a.smoothScroll.filterPath(e.pathname)||d)===d,p=f(e.hash);if(!c.scrollTarget&&(!n||!o||!p))l=!1;else{while(l&&j<h.length)g.is(f(h[j++]))&&(l=!1);while(l&&k<i.length)g.closest(i[k++]).length&&(l=!1)}l&&(b.preventDefault(),a.extend(m,c,{scrollTarget:c.scrollTarget||p,link:e}),a.smoothScroll(m))}),this}}),a.smoothScroll=function(b,c){var d,f,g,h,i=0,j="offset",k="scrollTop",l={},m=!1,n=[];typeof b=="number"?(d=a.fn.smoothScroll.defaults,g=b):(d=a.extend({},a.fn.smoothScroll.defaults,b||{}),d.scrollElement&&(j="position",d.scrollElement.css("position")=="static"&&d.scrollElement.css("position","relative")),g=c||a(d.scrollTarget)[j]()&&a(d.scrollTarget)[j]()[d.direction]||0),d=a.extend({link:null},d),k=d.direction=="left"?"scrollLeft":k,d.scrollElement?(f=d.scrollElement,i=f[k]()):(f=a("html, body").firstScrollable(),m=e&&"scrollTo"in window),l[k]=g+i+d.offset,d.beforeScroll.call(f,d),m?(n=d.direction=="left"?[l[k],0]:[0,l[k]],window.scrollTo.apply(window,n),d.afterScroll.call(d.link,d)):(h=d.speed,h==="auto"&&(h=l[k]||f.scrollTop(),h=h/d.autoCoefficent),f.stop().animate(l,{duration:h,easing:d.easing,complete:function(){d.afterScroll.call(d.link,d)}}))},a.smoothScroll.version=b,a.smoothScroll.filterPath=function(a){return a.replace(/^\//,"").replace(/(index|default).[a-zA-Z]{3,4}$/,"").replace(/\/$/,"")},a.fn.smoothScroll.defaults=c})(jQuery);

 jQuery(document).ready(function() {
	jQuery('a.top').smoothScroll();

		jQuery('a.share-btn').click(function() {
		jQuery.smoothScroll({
		scrollElement: jQuery('.share'),
		scrollTarget: '#'
		});
		return false;
	});

});

 if (document.documentElement.clientWidth > 1025) {
 jQuery(document).ready(function() {
	jQuery('a.site-nav-btn').smoothScroll();
});
}


/*---------------------------------------------------------------------------------------------
  Flexible width for embedded videos (see https://github.com/davatron5000/FitVids.js/)
----------------------------------------------------------------------------------------------*/
	jQuery(document).ready(function(){
		jQuery('#content').fitVids();
		jQuery('.widget').fitVids();
	});

/*---------------------------------------------------------------------------------------------
  Support Placeholder input text in IE (see https://github.com/danielstocks/jQuery-Placeholder)
----------------------------------------------------------------------------------------------*/
	jQuery(document).ready(function(){
		jQuery('input[placeholder], textarea[placeholder]').placeholder();
	});
	
/*--------------------------------------------------------------------------------------------
  Show/Hide effect site navigation and share-btn on mobile
----------------------------------------------------------------------------------------------*/
jQuery(document).ready(function(){
    	jQuery('#site-nav').hide();

    jQuery('a#mobile-menu-btn').click(function () {
    	jQuery( this ).find('#nav-icon').toggleClass('open');;
		jQuery( this ).next('#site-nav').slideToggle('200');       
    });
});



jQuery( document ).ready( 
	function()
	{
		jQuery("#sidebar").stick_in_parent({offset_top: 140});
		// Hide Header on on scroll down
		var didScroll;
		var navOffset = 140;
		var hoversNav = false;
		var lastScrollTop = 0;
		var delta = 5;
		var navbarHeight = jQuery( '#site-nav-container' ).outerHeight();

		jQuery(document).scroll(
			function( event )
			{
			    didScroll = true;			    
			}
		);
		jQuery(document).mousemove(
			function( event )
			{
			    if(event.pageY - window.scrollY <= navOffset && hoversNav == false){
			    	didScroll = true;
			    	hoversNav = true;
			    	jQuery( '#site-nav-container' ).removeClass( 'nav-up' ).addClass( 'nav-down' );			    				    	
			    }
			}
		);
		jQuery( '#site-nav-container' ).mouseleave(function(event){			
			if(window.scrollY > navOffset)
    			jQuery( this ).removeClass( 'nav-down' ).addClass( 'nav-up' );
    		hoversNav = false;
    	});

		function step() 
		{
		    if ( didScroll ) 
		    {
		        hasScrolled();
		        didScroll = false;		        
		    }
		    window.requestAnimationFrame( step );
		}

		window.requestAnimationFrame( step );
		function hasScrolled() 
		{
		    var st = jQuery( this ).scrollTop();
		    
		    // Make sure they scroll more than delta
		    if( Math.abs( lastScrollTop - st ) <= delta )
		        return;
		    
		    // If they scrolled down and are past the navbar, add class .nav-up.
		    // This is necessary so you never see what is "behind" the navbar.
		    if ( st > lastScrollTop && st > navbarHeight )
		    {
		        // Scroll Down
		        if(window.scrollY > navOffset)
		        	jQuery( '#site-nav-container' ).removeClass( 'nav-down' ).addClass( 'nav-up' );
		    } 
		    else 
		    {
		        // Scroll Up
		        if( st + jQuery( window ).height() < jQuery( document ).height() )
		        {
		        	if(window.scrollY > navOffset)
		            	jQuery( '#site-nav-container' ).removeClass( 'nav-up' ).addClass( 'nav-down' );
		        }
		    }	
		    lastScrollTop = st;
   		}
	}
);
