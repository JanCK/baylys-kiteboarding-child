<label for=""></label>

<style>
.smallframe {
margin: 20px 0;
padding: 10px 10px;
border: 1px; 
border-style: dashed;
border-color: #aaaaaa;
}
</style>
[checkbox* hiermit-bestaetige-ich-meine-bestellung-von-6-ausgaben-im-abo default:1 "JA, ich möchte keine Ausgabe mehr verpassen. Ich bestelle KITEBOARDING für mindestens 6 Ausgaben"]
<fieldset class="smallframe">
<legend>Wähle Dein Abo</legend>
Ab welcher Ausgabe soll der Aboversand erfolgen? [select* aboversand "Nächste Ausgabe" "Aktuelle Ausgabe"]

[radio auswahl default:1 "Jahresabo Deutschland inklusive KITEBOARDING Logo-Shirt für 25 Euro"]
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ich wähle das Shirt: [select shirt default:2 "Women /schwarz/Print türkis" "Men / schwarz/Print weiß" "Men / schwarz/Print schwarz" "Men / braun/Print schwarz" "Men / rot/Print schwarz"] in der Größe [radio shirtsize default:3 "XS" "S" "M" "L" "XL"]

[radio auswahl "Europa** ohne Präsent für 37,00"]
[radio auswahl " Weltweit** ohne Präsent für 49,00 Euro"]
<small>Alle Preise inkl. Versandkosten.</small>
</fieldset>

<fieldset class="smallframe">
<legend>Angaben zu Deiner Person</legend>
<label for="vorname">Vorname *</label>
[text* vorname placeholder "Vorname *"]
<label for="nachname">Nachname *</label>
[text* nachname placeholder "Nachname *"]
<label for="strasse">Straße und Hausnummer *</label>
[text* strasse placeholder "Straße und Hausnummer *"]
<label for="plzort">PLZ Ort *</label>
[text* plzort placeholder "PLZ Ort *"]
<label for="land">Land</label>
[text land placeholder "Land"]
<label for="telefon">Telefon</label>
[text telefon placeholder "Telefon"]
<label for="email">E-Mail *</label>
[email* email placeholder "E-Mail *"]
<label for="geborenam">Geboren am</label>
[text geborenam placeholder "Geboren am"]
</fieldset>

<fieldset class="smallframe">
<legend>Bankverbindung und Einzugsermächtigung - nur, wenn Du ein Konto in Deutschland hast</legend>

<label for="kontoinhaber">Konto-Inhaber</label>
[text kontoinhaber placeholder "Konto-Inhaber"]
<label for="bank">Bank</label>
[text bank placeholder "Bank"]
<label for="bankleitzahl">BIC</label>
[text bankleitzahl placeholder "BIC"]
<label for="kontonummer">IBAN</label>
[text kontonummer placeholder "IBAN"]
[checkbox auslandsabo-mit-deutschem-konto "Ich möchte ein Auslandsabo bestellen und bestätige dass ich dieses Konto in Deutschland besitze."]
[checkbox ich-erteile-eine-einzugsermaechtigung-an-die-kiteboarding "Ich bestätige die Erteilung einer Einzugsermächtigung. Der Bankeinzug erfolgt bei Zustellung des 1. Heftes."]

</fieldset>

<fieldset class="smallframe">
<legend>Widerspruchsrecht, Allgemeine Geschäfts- und Lieferbedingungen</legend>
[checkbox* ich-habe-das-widerrufsrecht-zur-kentniss-genommen "Ich habe zur Kenntnis genommen, dass ich diese Bestellung innerhalb von zwei Wochen bei Ihnen schriftlich widerrufen kann, wobei bereits die rechtzeitige Absendung meines Widerrufschreibens zur Wahrung der Frist ausreicht (Datum des Poststempels)."]
[checkbox* ich-nehmen-die-agbs-an "Hiermit bestätige ich die Allgemeinen Geschäfts- und Lieferbedingungen gelesen und zur Kenntnis genommen zu haben."]
</fieldset>
[textarea anmerkung 80x5 placeholder "Deine Anmerkung"]

[recaptcha]

[submit "Kaufen"]
<small>
<sup>*</sup> Pflichtfelder
<sup>**</sup> Bei Abonnements ausserhalb Deutschlands erfolgt der Versand ausschliesslich per Rechnung / Vorkasse.
Die Mandatsreferenznummer wird separat mitgeteilt.
<sup>***</sup> nur bei Abos innerhalb Deutschlands.
</small>