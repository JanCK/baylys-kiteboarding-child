<?php
// slow down heartbeat
add_filter('heartbeat_send', 'my_heartbeat_settings');
function my_heartbeat_settings($response)
{
    if ($_POST['interval'] != 60) {
        $response['heartbeat_interval'] = 60;
    }
    return $response;
}
// remove unwanted dashboard widgets for relevant users
function remove_dashboard_widgets()
{
    remove_meta_box('dashboard_primary', 'dashboard', 'normal'); // wp news
    remove_meta_box('dashboard_quick_press', 'dashboard', 'side');
    $user = wp_get_current_user();
    if (!$user->has_cap('manage_options')) {
        remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');
        remove_meta_box('dashboard_plugins', 'dashboard', 'normal');
        remove_meta_box('dashboard_secondary', 'dashboard', 'normal');
        remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');
        remove_meta_box('dashboard_recent_drafts', 'dashboard', 'side');
        remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
        remove_meta_box('dashboard_right_now', 'dashboard', 'normal');
    }
}
add_action('admin_init', 'remove_dashboard_widgets');
// load style
add_action('wp_enqueue_scripts', 'my_theme_enqueue_styles');
function my_theme_enqueue_styles()
{
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css', array(), '1.1.10');
    wp_enqueue_style('child-style', get_stylesheet_directory_uri().'/baylys-kiteboarding-child.css', array(), '0.0.8');
}

// load script
function my_theme_js()
{
    wp_enqueue_script('custom', get_stylesheet_directory_uri() . '/js/custom.js', array('jquery'), '1.1.4', true);
}
add_action('wp_enqueue_scripts', 'my_theme_js');

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');